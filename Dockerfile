FROM lscr.io/linuxserver/scrutiny:8e34ef8d-ls35
WORKDIR app/scrutiny
RUN \
  echo "**** install build packages ****" && \
  apk add -U --update --no-cache --virtual=build-dependencies \
    curl \
    gcc \
    git \
    go \
    musl-dev \
    nodejs \
    npm && \
  git clone https://github.com/AnalogJ/scrutiny.git && \
  cd scrutiny && \
  git checkout 8e34ef8d && \
  go get -u github.com/containrrr/shoutrrr@v0.6.0 && \
  go build -ldflags '-w -extldflags "-static"' -o scrutiny webapp/backend/cmd/scrutiny/scrutiny.go && \
  go build -ldflags '-w -extldflags "-static"' -o scrutiny-collector-selftest collector/cmd/collector-selftest/collector-selftest.go && \
  go build -ldflags '-w -extldflags "-static"' -o scrutiny-collector-metrics collector/cmd/collector-metrics/collector-metrics.go && \
  mv scrutiny /usr/local/bin/ && \
  mv scrutiny-collector-selftest /usr/local/bin/ && \
  mv scrutiny-collector-metrics /usr/local/bin/ && \
  cd / && \
  rm -rf scrutiny && \
  chmod +x /usr/local/bin/scrutiny* && \
  apk del --purge \
    build-dependencies && \
  rm -rf \
    /root/.cache \
    /tmp/* \
    /root/go \
    /root/.npm
