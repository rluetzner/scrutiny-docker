# Scrutiny-Docker

## WARNING!!!

This is built on top of commit 8e34ef8d of the Scrutiny repo. This is by far not the latest version.

## Intent

All I want is to send notifications to Matrix. Newer versions of containrrr/shoutrrr can do this, but even the latest Scrutiny release is stuck with shoutrrr 0.4.x. Matrix integration is only available with shoutrrr >= 0.5.x.

I'm also using a Raspberry Pi with arm32. Newer Scrutiny releases rely on InfluxDB instead of SQLite. Sadly InfluxDB does not provide binaries for arm32 and the Scrutiny project doesn't either. This is why I've patched an older release.

The base image is also from linuxserver.io who have since deprecated that image. I've been using that image for a while now and want to keep a stable version, because Scrutiny already does everything I want it to right now. The only thing missing was Matrix integration.
